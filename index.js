module.exports = {
  Upload: require("./Upload.js"),
  GraphQLUpload: require("./GraphQLUpload.js"),
  graphqlUploadExpress: require("./graphqlUploadExpress.js"),
};
